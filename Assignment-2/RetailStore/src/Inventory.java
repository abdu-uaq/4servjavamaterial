
public class Inventory {

	public static void main(String[] args) {
		
		Item i1=new Item("Stapler", 2.25, 15);
		Item i2=new Item("Paper", 32.99, 255);
		Item i3=new Item("Binder", 4.75, 9);
		
		String header = "Name \t\tPrice \t\tQuantity \tValue";
		
		System.out.println(header);
		
		//To create a seperator between header and items "==" 
		int count=0;
		do {
			System.out.print("==");
			count+=1;
		} while (count<header.length()-2);
		System.out.println("");
		
		double value [] =new double[3];
		value[0]=i1.getValue();
		value[1]=i2.getValue();
		value[2]=i3.getValue();

		
		System.out.println(i1.getName()+"\t\t"+"$"+i1.getPrice()+"\t\t"+i1.getQuantity()+"\t\t"+"$"+value[0]);
		System.out.println(i2.getName()+"\t\t"+"$"+i2.getPrice()+"\t\t"+i2.getQuantity()+"\t\t"+"$"+value[1]);
		System.out.println(i3.getName()+"\t\t"+"$"+i3.getPrice()+"\t\t"+i3.getQuantity()+"\t\t"+"$"+value[2]);

		double totalValue=0;
		for(int i=0;i<value.length;i++)
		{
			totalValue+=value[i];
		}
		System.out.println("\nTotal Inventory is $"+ totalValue );
		
	}

}
