
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;

public class Main {

	public static void main(String[] args) throws FileNotFoundException {

		Employee[] workers = new Employee[11];
		Scanner keyboard = new Scanner(System.in);

		DecimalFormat decFormat = new DecimalFormat("#.##");

		System.out.println("Number of students: ");
		int numberOfStudents = keyboard.nextInt();
		
		System.out.println("Number of staff: ");
		int numberOfClassifiedStaff  = keyboard.nextInt();

		System.out.println("Number of faculty: ");
		int numberOfFaculty = keyboard.nextInt();

		System.out.println("\nReading from the file 'allemployees.csv' provided >> \n  ");
		String nameOfFile = "C:/Users/404Error/eclipse-workspace/Assignment3/src/allemployees.csv";
		Scanner fileScanner = new Scanner(new File(nameOfFile));
		fileScanner.useDelimiter(",");
		

		// For Students
		for (int i = 0; i < numberOfStudents; i++) {
			workers[i] = new StudentEmployee(fileScanner.next(), fileScanner.next(), fileScanner.next(), fileScanner.next(), fileScanner.next(),
					fileScanner.next());
			System.out.println(workers[i]);
		}
		// For Classified Staff
		for (int i = numberOfStudents; i < (numberOfStudents + numberOfClassifiedStaff); i++) {
			workers[i] = new ClassifiedStaff(fileScanner.next(), fileScanner.next(), fileScanner.next(), fileScanner.next(), fileScanner.next());
			System.out.println(workers[i]);
		}
		// For Faculty
		for (int i = (numberOfStudents + numberOfClassifiedStaff); i < (numberOfStudents + numberOfClassifiedStaff + numberOfFaculty); i++) {
			workers[i] = new Faculty(fileScanner.next(), fileScanner.next(), fileScanner.next(), fileScanner.next(), fileScanner.next(), fileScanner.next());
			System.out.println(workers[i]);
		}
		fileScanner.close();
		keyboard.close();
		
		
		System.out.println("\nPay for two-week pay period");
		System.out.println("===========================");
		
		for (int i = 0; i < 11; i++) {
			if (workers[i].isWorking()) {
				System.out.println(workers[i].getEmployeeName() + "\t$" + decFormat.format(workers[i].getPay()) );
			}
		}
	}

}
